let weatherWidget = document.querySelector('.weather');
let frame = document.querySelector('.frame');

const selectors = {
    city: 'weather-city',
    rain: 'weather-rain-chance',
    natural: 'weather-natural-condition',
    real: 'weather-real-feel',
};

((global, widget, frame, selectors) => {

    /*
    function appendTextNode(element, string) {
        element.appendChild(
            document.createTextNode(string)
        );
    }

    function prependTextNode(element, string, list = true) {
        element.innerHTML = (!list ? string : (string + ': ')) + element.innerHTML;
    }
    */
    function updateWeather(el, weather) {
        el.querySelector(selectors.city).innerHTML = weather.address.city;
        el.querySelector(selectors.rain).innerHTML = weather.chanceofrain;
        el.querySelector(selectors.natural).innerHTML = weather.naturalCondition.charAt(0).toLowerCase()
            + weather.naturalCondition.substring(1);
        el.querySelector(selectors.real).innerHTML = weather.feelsLike;
    }

    function initWeather(el) {
        el.style.background = weatherBackground;
        el.style.color = weatherColor;
        el.style.fontSize = weatherFontSize;
        el.style.height = weatherBoxHeight;
        el.style.width = weatherBoxWidth;
        el.style.padding = weatherBoxPadding;
        frame.style.color = weatherColor;
        frame.style.fontSize = weatherFontSize;
        frame.style.height = frameHeight;
        frame.style.width = frameWidth;
        frame.style.padding = weatherBoxPadding;
        console.log(el);
    }

    global.addEventListener("load", function () {
        if (!document) return;
        document.body.style.width = '100%';
        document.body.style.height = '100%';
    }, false);

    global.updateWeather = updateWeather;
    initWeather(widget);


})(window, weatherWidget, frame, selectors);

function mainUpdate(type) {
    if (type !== "weather") return;
    updateWeather(weatherWidget, weather);
}